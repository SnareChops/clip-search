package main

import (
	"encoding/json"
	"net/http"
	"time"
)

func requestAllClips(token string, channelId string) {
	clips, cursor, err := requestClips(token, channelId)
}

func requestClips(token string, channelId string, after string) ([]Clip, string, error) {
	url := "https://api.twitch.tv/helix/clips?broadcaster_id=" + channelId + "&first=100"
	if after != "" {
		url += "&after=" + after
	}
	request, err := newTwitchRequest(token, http.MethodGet, url, nil)
	if err != nil {
		return nil, "", err
	}

	var body []byte
	_, err = request.Body.Read(body)
	if err != nil {
		return nil, "", err
	}

	r := ClipResponse{}
	err = json.Unmarshal(body, &r)
	if err != nil {
		return nil, "", err
	}

}

type Clip struct {
	ID              string     `json:"id"`
	Url             string     `json:"url"`
	EmbedUrl        string     `json:"embed_url"`
	BroadcasterID   string     `json:"broadcaster_id"`
	BroadcasterName string     `json:"broadcaster_name"`
	CreatorID       string     `json:"creator_id"`
	VideoID         string     `json:"video_id"`
	GameID          string     `json:"game_id"`
	Language        string     `json:"language"`
	Title           string     `json:"title"`
	ViewCount       string     `json:"view_count"`
	CreatedAt       *time.Time `json:"created_at"`
	ThumbnailUrl    string     `json:"thumbnail_url"`
}

type ClipResponse struct {
	Data       []Clip     `json:"data"`
	Pagination Pagination `json:"pagination"`
}

type Pagination struct {
	Cursor string `json:"cursor"`
}

func NewClip(data map[string]interface{}) *Clip {
}
