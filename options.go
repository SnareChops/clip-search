package main

import (
	"flag"
	"os"
)

type Options struct {
	Channel string
	Scan    bool
	Token   string
}

func getOptions() *Options {
	channel := flag.String("channel", "", "Channel to search for clips")
	scan := flag.Bool("scan", false, "Scan channel for new non-cached clips")

	flag.Parse()

	return &Options{
		Channel: *channel,
		Scan:    *scan,
		Token:   os.Getenv("TWITCH_TOKEN"),
	}
}
