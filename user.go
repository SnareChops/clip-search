package main

import (
	"encoding/json"
	"net/http"
)

func getBroadcasterId(token string, name string) (string, error) {
	request, err := newTwitchRequest(token, http.MethodGet, "https://api.twitch.tv/helix/users?login="+name, nil)
	if err != nil {
		return "", err
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return "", err
	}

	var body []byte
	_, err = response.Body.Read(body)
	if err != nil {
		return "", err
	}

	j := map[string]interface{}{}
	err = json.Unmarshal(body, &j)
	if err != nil {
		return "", err
	}

	return j["data"].([]interface{})[0].(map[string]interface{})["id"].(string), nil
}
