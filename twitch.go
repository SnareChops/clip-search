package main

import (
	"io"
	"net/http"
)

func newTwitchRequest(token string, method string, url string, body io.Reader) (*http.Request, error) {
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	request.Header.Add("Authorization", "Bearer: "+token)
	return request, nil
}
