package main

import (
	"errors"
)

func main() {
	options := getOptions()

	if options.Scan {
		err := scan(options)
		if err != nil {
			panic(err)
		}
	}

}

func scan(options *Options) error {
	if options.Channel == "" {
		return errors.New("-channel required")
	}

	return nil
}
